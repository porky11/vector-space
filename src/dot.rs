use crate::VectorSpace;

/// This trait defines the dot product.
pub trait DotProduct<T = Self>: VectorSpace {
    /// The output type of the dot product.
    type Output;

    /// The dot product.
    fn dot(self, other: T) -> <Self as DotProduct<T>>::Output;
}

impl DotProduct for f32 {
    type Output = Self;
    fn dot(self, other: Self) -> Self {
        self * other
    }
}
impl DotProduct for f64 {
    type Output = Self;
    fn dot(self, other: Self) -> Self {
        self * other
    }
}
