use num_traits::{real::Real, Zero};
use std::{
    cmp::PartialOrd,
    ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign},
};

/// This trait specifies some type to be a vector type.
/// It specifies the scalar type and is required for other vector types.
pub trait VectorSpace: Copy + Zero + PartialEq
where
    Self: Add<Output = Self>
        + Sub<Output = Self>
        + Mul<<Self as VectorSpace>::Scalar, Output = Self>
        + Div<<Self as VectorSpace>::Scalar, Output = Self>
        + Neg<Output = Self>,
{
    /// The scalar type of the vector space.
    type Scalar: Real + PartialOrd;
}

impl VectorSpace for f32 {
    type Scalar = Self;
}
impl VectorSpace for f64 {
    type Scalar = Self;
}

/// This trait is automatically implemented for vector spaces, which also implement assignment operations.
pub trait VectorSpaceAssign:
    VectorSpace + AddAssign + SubAssign + MulAssign<Self::Scalar> + DivAssign<Self::Scalar>
{
}
impl<T> VectorSpaceAssign for T where
    T: VectorSpace + AddAssign + SubAssign + MulAssign<Self::Scalar> + DivAssign<Self::Scalar>
{
}
