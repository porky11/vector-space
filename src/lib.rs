#![deny(missing_docs)]
/*!
This crate contains new traits useful for working with vector spaces.
They have default implementations, so you can just implement them to get a lot of useful methods for free for your vector type.
But you can also just implement the methods yourself.

You can also define some library in terms of these traits instead of using a specific vector math implementation, so the user can choose, which one to use, or simply add multiple vector math libraries which implement these traits yourself by using this library.
**/

mod dot;
mod inner;
mod outer;
mod vector;

pub use dot::DotProduct;
pub use inner::InnerSpace;
pub use outer::OuterProduct;
pub use vector::{VectorSpace, VectorSpaceAssign};

use std::ops::{Add, Sub};

/// The linear interpolation of two points.
pub fn interpolate<T>(a: T, b: T, ratio: <<T as Sub>::Output as VectorSpace>::Scalar) -> T
where
    T: Copy + Sub + Add<<T as Sub>::Output, Output = T>,
    <T as Sub>::Output: VectorSpace,
{
    a + (b - a) * ratio
}

/// The distance between two points.
pub fn distance<T: Sub>(a: T, b: T) -> <T::Output as VectorSpace>::Scalar
where
    T::Output: InnerSpace,
{
    (b - a).magnitude()
}

/// The squared distance between two points.
pub fn distance2<T: Sub>(a: T, b: T) -> <T::Output as VectorSpace>::Scalar
where
    T::Output: InnerSpace,
{
    (b - a).magnitude2()
}
