use crate::VectorSpace;

/// This trait defines the outer product.
pub trait OuterProduct<T = Self>: VectorSpace {
    /// The output type of the outer product.
    type Output;

    /// The outer product.
    fn outer(self, other: T) -> <Self as OuterProduct<T>>::Output;
}

impl OuterProduct for f32 {
    type Output = Self;
    fn outer(self, other: Self) -> Self {
        self * other
    }
}

impl OuterProduct for f64 {
    type Output = Self;
    fn outer(self, other: Self) -> Self {
        self * other
    }
}
